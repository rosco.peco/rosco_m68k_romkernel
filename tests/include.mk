tests/list.bin: tests/list.elf
	$(OBJCOPY) -O binary $< $@

tests/list.elf: tests/list.o list.o
	$(LD) $(LDFLAGS) $(GCC_LIBS) $^ -o $@ $(LIBS)
	$(SIZE) $@
	-chmod a-x $@

tests/pmm.bin: tests/pmm.elf
	$(OBJCOPY) -O binary $< $@

tests/pmm.elf: tests/pmm.o kmachine.o list.o pmm.o
	$(LD) $(LDFLAGS) $(GCC_LIBS) $^ -o $@ $(LIBS)
	$(SIZE) $@
	-chmod a-x $@

tests/bitmap.bin: tests/bitmap.elf
	$(OBJCOPY) -O binary $< $@

tests/bitmap.elf: tests/bitmap.o bitmap.o
	$(LD) $(LDFLAGS) $(GCC_LIBS) $^ -o $@ $(LIBS)
	$(SIZE) $@
	-chmod a-x $@

tests/slab.bin: tests/slab.elf
	$(OBJCOPY) -O binary $< $@

tests/slab.elf: tests/slab.o kmachine.o list.o pmm.o slab.o
	$(LD) $(LDFLAGS) $(GCC_LIBS) $^ -o $@ $(LIBS)
	$(SIZE) $@
	-chmod a-x $@

tests/units.bin: tests/units.elf
	$(OBJCOPY) -O binary $< $@

ALLTEST_OBJS=tests/rtest/suite.o						\
			 tests/rtest/list.o 						\
			 tests/rtest/pmm.o							\
			 tests/rtest/bitmap.o						\
			 tests/rtest/slab.o							\
			 tests/rtest/interrupts.o					\
			 list.o										\
			 pmm.o										\
			 bitmap.o									\
			 slab.o										\
			 kmachine.o

tests/units.elf: $(ALLTEST_OBJS)
	$(LD) $(LDFLAGS) $(GCC_LIBS) $^ -o $@ $(LIBS)
	$(SIZE) $@
	-chmod a-x $@

test: tests/units.bin
	$(R68K_DIR)/r68k $<
